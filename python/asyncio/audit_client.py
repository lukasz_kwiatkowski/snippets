import asyncio
import logging
import socket
from asyncio import AbstractEventLoop
from asyncio.exceptions import CancelledError
import signal
import six
import msgpack

from rich.console import Console
from rich.table import Table
from rich.live import Live
import contextlib
import time
from rich import box
from rich.align import Align


console = Console()

def make_table(infos):
    table = Table(title="Noble Gases")

    columns = [
        {"heading": "Ip",           "style": "yellow", "justify": "center"},
        {"heading": "Port",         "style": "yellow", "justify": "center"},
        {"heading": "type",         "style": "magenta", "justify": "center"},
        {"heading": "Total data",   "style":  "green",  "justify": "right"}
    ]
    for column in columns:
        table.add_column(column["heading"], style=column["style"], justify=column["justify"])

    # for box_style in [box.SQUARE,box.MINIMAL,box.SIMPLE,box.SIMPLE_HEAD]:
    #     table.box = box_style

    for info in infos:
        print(info)
        table.add_row(
            info["ip"],
            str(info["port"]),
            info["type"],
            str(info["total_data"])
        )

    table_centered = Align.center(table)
    return table_centered

class MessageIterator:
    def __init__(self, read_call, buff_size=1024):
        self.read_call = read_call
        self.buff_size = buff_size
        self.unpacker = msgpack.Unpacker()

    def __aiter__(self):
        return self

    async def __anext__(self):
        while True:
            buf = await self.read_call(self.buff_size)
            if not buf:
                raise StopAsyncIteration
            try:
                self.unpacker.feed(buf)
                o = self.unpacker.unpack()
                if o: return o
            except msgpack.exceptions.OutOfData:
                continue

class AuditClient():

    def __init__(self, ip, audit_port, log):
        self.ip = ip
        self.audit_port = audit_port
        self.loop = asyncio.get_event_loop()
        self.log = log
        
        self.server = None
        self.sock_port = None
        self.sock_ip = None

    async def _reconnect(self):
        server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server.setblocking(False)

        while True:
            try:
                self.log.info(f'try connect to server at [{self.ip} {self.audit_port}]')
                await self.loop.sock_connect(server, (self.ip, self.audit_port))
                self.sock_ip, self.sock_port = server.getsockname()
                return server
            except socket.error:
                await asyncio.sleep(0.5)

    async def start(self):
        self.server = await self._reconnect()
        self.log.info(f'[{self.sock_ip} {self.sock_port}] start audit client')

    async def run(self):
        with Live(make_table([]), screen=True) as live:
            with contextlib.suppress(KeyboardInterrupt):
                async for msg in MessageIterator(read_call=self.server_readall):
                    live.update(make_table(msg))

    def stop(self):
        self.log.info(f'[{self.sock_ip} {self.sock_port}] Closing connection')
        self.server.close()

    async def __aenter__(self):
        await self.start()
        return self

    async def __aexit__(self, exc_type,  exc_val, exc_tb):
        self.stop()

        if exc_type and exc_type not in [CancelledError]:
            six.reraise(exc_type, exc_val, exc_tb)

    async def server_readall(self, size):
        return await self.loop.sock_recv(self.server, size)


async def async_main(log):
    
    async with AuditClient(ip='127.0.0.1', audit_port=8001, log=log) as audit_client:
        await audit_client.run()


# class GracefulExit(SystemExit):
#     pass


# def shutdown():
#     raise GracefulExit()

def main():

    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.DEBUG)

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    logger.addHandler(stream_handler)

    # loop = asyncio.new_event_loop()


    asyncio.run(async_main(logger))


    # for signame in {'SIGINT', 'SIGTERM'}:
    #     loop.add_signal_handler(getattr(signal, signame), shutdown)

    # try:
    #     fun_coro = async_main(logger)
    #     loop.run_until_complete(fun_coro)
    # except GracefulExit:
    #     logger.info(f'except')

if __name__ == '__main__':
    main()













# async def async_main(log):
    
#     server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#     server.connect(('localhost', 8000))
#     data = b'12345'
#     ret_data = b''

#     loop = asyncio.get_event_loop()

#     try:
#         await loop.sock_sendall(server, data)
#         ret_data = await loop.sock_recv(server, 1024)
#         response = ret_data.decode('utf8')
#         print(response)
#         await loop.sock_sendall(server, b'exit\r\n')
#     except Exception as e:
#         raise e 
#     finally:
#         server.close()
