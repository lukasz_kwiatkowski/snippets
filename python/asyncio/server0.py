import asyncio
import logging
import socket
from asyncio import AbstractEventLoop
from asyncio.exceptions import CancelledError
import signal
import six
import msgpack




class EchoClient():

    def __init__(self, connection, loop, log):
        self.connection = connection
        self.loop = loop
        self.log = log
        self.total_data = 0

        self.peer_ip, self.peer_port = self.connection.getpeername()

    def get_info(self):
        return {
            "type": self.__class__.__name__,
            "ip": self.peer_ip,
            "port":self.peer_port,
            "total_data": self.total_data 
        }

    def start(self):
        self.log.info(f'[{self.peer_ip} {self.peer_port}] initialize client')

    async def run(self):
        while data := await self.loop.sock_recv(self.connection, 1024):
            if data == b'boom\r\n':
                raise Exception('boom')

            if data == b'exit\r\n':
                self.log.info(f'[{self.peer_ip} {self.peer_port}] remote send "exit"')
                return

            await self.loop.sock_sendall(self.connection, data)
            self.total_data += max(0, len(data) -2)

    def stop(self):
        self.log.info(f'[{self.peer_ip} {self.peer_port}] Closing connection')
        self.connection.close()

    async def __aenter__(self):
        self.start()
        return self

    async def __aexit__(self, exc_type,  exc_val, exc_tb):
        self.stop()

        if exc_type and exc_type not in [CancelledError]:
            six.reraise(exc_type, exc_val, exc_tb)

class AuditClient():

    def __init__(self, connection, loop, log, info_call):
        self.connection = connection
        self.loop = loop
        self.info_call = info_call
        self.packer = msgpack.Packer()
        self.total_data = 0
        self.log = log

        self.peer_ip, self.peer_port = self.connection.getpeername()

    def get_info(self):
        return {
            "type": self.__class__.__name__,
            "ip": self.peer_ip,
            "port":self.peer_port,
            "total_data": self.total_data 
        }

    def start(self):
        self.log.info(f'[{self.peer_ip} {self.peer_port}] initialize client')

    async def run(self):
        while True:
            self.log.info(f'[{self.peer_ip} {self.peer_port}] send audit data:"')
            info = self.info_call()
            self.log.info(info)

            buff = self.packer.pack(info)
            await self.loop.sock_sendall(self.connection, buff)
            self.total_data += len(buff)
            await asyncio.sleep(0.5)

    def stop(self):
        self.log.info(f'[{self.peer_ip} {self.peer_port}] Closing connection')
        self.connection.close()

    async def __aenter__(self):
        self.start()
        return self

    async def __aexit__(self, exc_type,  exc_val, exc_tb):
        self.stop()

        if exc_type and exc_type not in [CancelledError]:
            six.reraise(exc_type, exc_val, exc_tb)

class Server():

    def __init__(self, name, ip, api_port, log):
        self.name = name
        self.ip = ip
        self.api_port = api_port
        self.log = log

        self.clients_list = []
        self.server_socket = None

    async def __aenter__(self):
        loop = asyncio.get_event_loop()

        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        self.server_socket.setblocking(False)
        self.server_socket.bind((self.ip, self.api_port))
        self.server_socket.listen()

        return self

    async def run(self, client_type, *client_args, **client_kwargs):
        loop = asyncio.get_event_loop()

        self.server_socket.listen()

        while True:
            connection, address = await loop.sock_accept(self.server_socket)
            connection.setblocking(False)

            ct = client_type(connection, loop, self.log, *client_args, **client_kwargs)
            tsk = self.run_client(ct)

            task = asyncio.create_task(tsk)
            self.clients_list.append((task,ct))

    async def __aexit__(self, exc_type,  exc_val, exc_tb):
        self.server_socket.close()

    async def run_client(self, client):
        try:
            async with client as c:
                await c.run()

        except Exception as e:
            self.log.exception(f'Client: {client} exception {e}')
        finally:
            for index,(_,cl) in enumerate(self.clients_list):
                if client == cl:
                    self.clients_list.pop(index)
                    break

    def get_info(self):
        res = []
        for task,client in self.clients_list:
            info= client.get_info()
            res.append(info)

        return res

class CombineInfo():
    def __init__(self, *servers):
        self.servers = servers

    def __call__(self):
        res = []
        for srv in self.servers:
            res += srv.get_info()

        return res

async def async_main(log):
    
    async with (
    Server(name='asd', ip='127.0.0.1', api_port=8000, log=log) as api_server,
    Server(name='asd', ip='127.0.0.1', api_port=8001, log=log) as audit_server):
        task0 = asyncio.create_task(api_server.run(EchoClient))

        task1 = asyncio.create_task(audit_server.run(AuditClient, CombineInfo(api_server,audit_server)))
        await task0
        await task1

# async def async_main(log):
    
#     async with Server(name='asd', ip='127.0.0.1', api_port=8000, log=log) as api_server:
#         await api_server.run(client_type=AuditClient)



class GracefulExit(SystemExit):
    pass


def shutdown():
    raise GracefulExit()

def main():

    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.DEBUG)

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    logger.addHandler(stream_handler)

    # loop = asyncio.new_event_loop()


    asyncio.run(async_main(logger))


    # for signame in {'SIGINT', 'SIGTERM'}:
    #     loop.add_signal_handler(getattr(signal, signame), shutdown)

    # try:
    #     fun_coro = async_main(logger)
    #     loop.run_until_complete(fun_coro)
    # except GracefulExit:
    #     logger.info(f'except')

if __name__ == '__main__':
    main()










# async def echo(connection: socket, loop: AbstractEventLoop, log) -> None:
#     try:
#         peername = connection.getpeername()
#         sockname = connection.getsockname()
#         while data := await loop.sock_recv(connection, 1024):
#             if data == b'boom\r\n':
#                 raise Exception('boom')
#             if data == b'exit\r\n':
#                 return
#             await loop.sock_sendalll(connection, data)
#     except Exception as ex:
#         log.exception(ex)
#     finally:
#         log.info('Closing connection')
#         connection.close()

# async def listen_for_connection(server_socket: socket,
#                                 loop: AbstractEventLoop):
#     while True:
#         connection, address = await loop.sock_accept(server_socket)
#         connection.setblocking(False)
#         print(f"Got a connection from {address}")
#         asyncio.create_task(echo(connection, loop))

# async def run_echo_server(ip, port: int, log):
#     clients_list = []
    
#     loop = asyncio.get_event_loop()

#     server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#     server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

#     server_address = (ip, port)
#     server_socket.setblocking(False)
#     server_socket.bind(server_address)
#     server_socket.listen()

#     while True:
#         connection, address = await loop.sock_accept(server_socket)
#         connection.setblocking(False)
#         print(f"Got a connection from {address}")
#         task = asyncio.create_task(echo(connection, loop, log))
#         clients_list.append(task)











# async def fun(log):
#     try:
#         i = 0
#         while True:
#             await asyncio.sleep(1)
#             log.info(f'fun: {i}')
#     except Exception as ex:
#         log.exception(ex)

