import argparse
import time
import systemd.daemon
import logging
import signal
from systemd.journal import JournalHandler
from stringcolor import * 
import random

class GracefulKiller:
  kill_now = False
  def __init__(self, log):
    self.log = log
    signal.signal(signal.SIGINT, self.get_sigint)
    signal.signal(signal.SIGTERM, self.get_sigterm)

  def get_sigint(self, signum, frame):
    self.log.info(bold(f"get signal: {signum} , stop service").cs("green", "yellow"))
    self.kill_now = True

  def get_sigterm(self, signum, frame):
    self.log.info(bold(f"get signal: {signum} , stop service").cs("green", "yellow"))
    self.kill_now = True

def main():

    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.DEBUG)

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    logger.addHandler(JournalHandler())
    # logger.addHandler(stream_handler)

    parser = argparse.ArgumentParser()
    parser.add_argument("--nr", type=str)
    args = parser.parse_args()

    random.seed()

    systemd.daemon.notify('READY=1')

    killer = GracefulKiller(log=logger)
    while not killer.kill_now:
        i =  random.randint(0,3)

        if i ==0: logger.info(bold(f"Worker {args.nr} | Error: asdf").cs("red", "yellow"))
        elif i ==1: logger.info(cs(f"Worker {args.nr} | Info: asdf", "rgb(254, 254, 254)").bold())  
        elif i ==2: logger.info(cs(f"Worker {args.nr} | Success: asdf", "rgb(0, 254, 0)"))
        time.sleep(1)

if __name__ == '__main__':
    main()

