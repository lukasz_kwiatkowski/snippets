
import msgpack


dct = {
    "var1": "qwert",
    "var2": 1234,
    "var3": "234"
}


packer = msgpack.Packer()
buff = bytearray(packer.pack(dct))


def read_form_sock():
    global buff

    if not buff:
        raise Exception('Out of data')

    byte = buff[0]
    del buff[0]
    return byte.to_bytes()

# print(read_form_sock())
# print(read_form_sock())


unpacker = msgpack.Unpacker()
index = 1
while True:
    print(f"Read attempt {index}")
    index += 1
    buf = read_form_sock()

    if not buf:break
    
    unpacker.feed(buf)
    try:
        o = unpacker.unpack()
        print(o)
    except msgpack.exceptions.OutOfData:
        continue