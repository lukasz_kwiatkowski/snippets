# import functools

# def decorator(func):
#     @functools.wraps(func)
#     def wrapper_decorator(*args, **kwargs):
#         print('Do something before')
#         value = func(*args, **kwargs)
#         print('Do something after')
#         return value
#     return wrapper_decorator

# def fun():
#     print('fun')

# fun = decorator(fun)
# fun()

# #################################################################################################

# import functools

# def decorator(func):
#     @functools.wraps(func)
#     def wrapper_decorator(*args, **kwargs):
#         print('Do something before')
#         value = func(*args, **kwargs)
#         print('Do something after')
#         return value
#     return wrapper_decorator

# @decorator
# def bun():
#     print('bun')

# bun()

# #################################################################################################

# import functools

# def decorator(func):
#     @functools.wraps(func)
#     def wrapper_decorator(*args, **kwargs):
#         print('Do something before')
#         value = func(*args, **kwargs)
#         print('Do something after')
#         return value
#     return wrapper_decorator

# @decorator
# @decorator
# def un():
#     print('un')

# un()

# #################################################################################################

# import functools

# def repeat(num_times):
#     def decorator_repeat(func):
#         @functools.wraps(func)
#         def wrapper_repeat(*args, **kwargs):
#             for _ in range(num_times):
#                 value = func(*args, **kwargs)
#             return value
#         return wrapper_repeat
#     return decorator_repeat

# @repeat(num_times=3)
# def fun():
#     print('fun')

# # rep_decorator = repeat(num_times=3)
# # fun = rep_decorator(fun)

# fun()

# #################################################################################################

import functools

# class CountCalls:
#     def __init__(self, func):
#         functools.update_wrapper(self, func)
#         self.func = func

#     def __call__(self, *args, **kwargs):
#         print('Do something before')
#         value = self.func(*args, **kwargs)
#         print('Do something after')
#         return value

# @CountCalls
# def fun():
#     print('fun')

# fun()

# #################################################################################################


# class Wait():

#     def __init__(self, **kwargs):

#         self.message = kwargs['message'] if 'message' in kwargs else 'Default message' 
#         self.duration = kwargs['duration'] if 'duration' in kwargs else 5 

#     def execute(self):
#         print(f"message: {self.message}")
#         print(f"duration: {self.duration}")

# def with_args(**kwargs):
#     def decorator_with_args(cls):
#         orig_init = cls.__init__

#         def __init__(self):

#             orig_init(self, **kwargs)

#         cls.__init__ = __init__
#         return cls

#     return decorator_with_args

# W = with_args(duration=45, message='sdfg')(Wait)

# w =W()

# # w = Wait(duration=45, message='sdfg')


# w.execute()
