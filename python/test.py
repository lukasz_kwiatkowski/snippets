import os
import pathlib
import sys

import asyncio


class EchoClientProtocol(asyncio.Protocol):
    def __init__(self, message, on_con_lost):
        self.message = message
        self.on_con_lost = on_con_lost

    def connection_made(self, transport):
        transport.write(self.message.encode())
        print('Data sent: {!r}'.format(self.message))

    def data_received(self, data):
        print('Data received: {!r}'.format(data.decode()))

    def connection_lost(self, exc):
        print('The server closed the connection')
        self.on_con_lost.set_result(True)


async def main():
    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    on_con_lost = loop.create_future()
    message = 'Hello World!'

    transport, protocol = await loop.create_connection(
        lambda: EchoClientProtocol(message, on_con_lost),
        '127.0.0.1', 8000)

    # Wait until the protocol signals that the connection
    # is lost and close the transport.
    try:
        await on_con_lost
    finally:
        transport.close()


asyncio.run(main())











# class EchoClient():

#     def __init__(self):
#         pass

#     def __enter__(self):
#         return self


#     def __exit__(self, exc_type,  exc_val, exc_tb):
#         print(exc_type)
#         print(exc_val)
#         print(exc_tb)




# with EchoClient() as ec:
#     print('jngjh')
#     raise Exception('sdfgh')









# from rich import print
# from rich.filesize import decimal
# from rich.markup import escape
# from rich.text import Text
# from rich.tree import Tree


# def walk_directory(directory: pathlib.Path, tree: Tree) -> None:
#     """Recursively build a Tree with directory contents."""
#     # Sort dirs first then by filename
#     paths = sorted(
#         pathlib.Path(directory).iterdir(),
#         key=lambda path: (path.is_file(), path.name.lower()),
#     )
#     for path in paths:
#         # Remove hidden files
#         if path.name.startswith("."):
#             continue
#         if path.is_dir():
#             style = "dim" if path.name.startswith("__") else ""
#             branch = tree.add(
#                 f"[bold magenta]:open_file_folder: [link file://{path}]{escape(path.name)}",
#                 style=style,
#                 guide_style=style,
#             )
#             walk_directory(path, branch)
#         else:
#             text_filename = Text(path.name, "green")
#             text_filename.highlight_regex(r"\..*$", "bold red")
#             text_filename.stylize(f"link file://{path}")
#             file_size = path.stat().st_size
#             text_filename.append(f" ({decimal(file_size)})", "blue")
#             icon = "🐍 " if path.suffix == ".py" else "📄 "
#             tree.add(Text(icon) + text_filename)


# try:
#     directory = os.path.abspath(sys.argv[1])
# except IndexError:
#     print("[b]Usage:[/] python tree.py <DIRECTORY>")
# else:
#     tree = Tree(
#         f":open_file_folder: [link file://{directory}]{directory}",
#         guide_style="bold bright_blue",
#     )
#     walk_directory(pathlib.Path(directory), tree)
#     print(tree)


# from rich.console import Console
# from rich.table import Table

# table = Table(title="Todo List")

# table.add_column("S. No.", style="cyan", no_wrap=True)
# table.add_column("Task", style="magenta")
# table.add_column("Status", justify="right", style="green")

# table.add_row("1", "Buy Milk", "X")
# table.add_row("2", "Buy Bread", "O")
# table.add_row("3", "Buy Jam", "X")

# console = Console()
# console.print(table)
